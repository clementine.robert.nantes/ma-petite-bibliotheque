module.exports = {
    devServer: {
        disableHostCheck: true,
        port: 4000,
        public: '0.0.0.0:4000'
    },
    publicPath: "/",
    pwa: {
        themeColor: '#1F2937',
        msTileColor: '#000000',
        iconPaths: {
        msTileImage: 'img/icons/mstile-150x150.png'
        }
    }
}