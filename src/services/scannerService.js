import { BarcodeScanner } from "@capacitor-community/barcode-scanner";

const startScan = async () => {
  // eslint-disable-next-line no-async-promise-executor
  return new Promise(async (resolve, reject) => {
    didUserGrantPermission().catch(() => {
      resolve("web");
      console.log("ouvrir le scan web");
    });

    BarcodeScanner.hideBackground(); // make background of WebView transparent

    const result = await BarcodeScanner.startScan(); // start scanning and wait for a result

    // if the result has content
    if (result.hasContent) {
      resolve(result.content);
    } else {
      reject(false);
    }
  });
};

const didUserGrantPermission = async () => {
  // vérifier si l'utilisateur a déjà accordé l'autorisation
  // eslint-disable-next-line no-async-promise-executor
  return new Promise(async (resolve, reject) => {
    BarcodeScanner.checkPermission({ force: false })
      .then((status) => {
        if (status.granted) {
          // autorisation accordée par l'utilisateur
          resolve(true);
        }

        if (status.denied) {
          // autorisation refusée par l'utilisateur
          resolve(false);
        }

        if (status.asked) {
          // le système a demandé à l'utilisateur l'autorisation pendant cet appel
          // uniquement possible lorsque la force est définie sur true
        }

        if (status.neverAsked) {
          // l'utilisateur n'a pas été invité à obtenir cette autorisation auparavant
          // il est conseillé de montrer à l'utilisateur une sorte d'invite
          // de cette façon, vous ne perdrez pas votre seule chance de demander l'autorisation
          const c = confirm(
            "Nous avons besoin de votre autorisation pour utiliser votre appareil photo pour pouvoir scanner des codes-barres"
          );
          if (!c) {
            resolve(false);
          }
        }

        if (status.restricted || status.unknown) {
          // iOS uniquement
          // signifie probablement que l'autorisation a été refusée
          resolve(false);
        }
      })

      .catch((err) => {
        reject(err);
      });

    // l'utilisateur n'a pas refusé l'autorisation
    // mais l'utilisateur n'a pas non plus encore accordé l'autorisation
    // alors demandez-le
    await BarcodeScanner.checkPermission({ force: true })
      .then((statusRequest) => {
        if (statusRequest.asked) {
          // le système a demandé à l'utilisateur l'autorisation pendant cet appel
          // uniquement possible lorsque la force est définie sur true
        }

        if (statusRequest.granted) {
          // l'utilisateur a accordé l'autorisation maintenant
          resolve(true);
        }

        // l'utilisateur n'a pas accordé l'autorisation, il doit donc avoir refusé la demande
        resolve(false);
      })
      .catch((err) => {
        reject(err);
      });
  });
};

const stopScan = () => {
  BarcodeScanner.showBackground();
  return new Promise((resolve, reject) => {
    BarcodeScanner.stopScan()
      .then(() => {
        resolve(true);
      })
      .catch((err) => {
        reject(err);
      });
  });
};

export { startScan, stopScan };
