import { callApi } from "./apiService";
import { Constantes } from "../common/constantes";
import firebase from "firebase/compat/app";

const db = firebase.firestore();

async function searchABookByTitle(title, startIndex) {
  let url = Constantes.URL + "?q=" + title + "&maxResults=30&startIndex=" + startIndex;
  return await callApi(url);
}

async function serchABookByIsbn(isbn) {
  let url = Constantes.URL + "?q=isbn:" + isbn;
  return await callApi(url);
}

async function getMyBooks(idUser) {
  return new Promise((resolve) => {
    let arrayBooks = [];
    db.collection("userBook")
      .where("idUser", "==", idUser)
      .get()
      .then((snapshot) => {
        snapshot.forEach((doc) => {
          arrayBooks.push(doc.data());
        });
        resolve(arrayBooks);
      });
  });
}

export { searchABookByTitle, serchABookByIsbn, getMyBooks };
