import { Constantes } from "../common/constantes";
import axios from "axios";

/**
 * Retourne l'api key pour accéder à une requête
 * @returns
 */
function getApiKey() {
  return Constantes.KEY + Constantes.API_KEY;
  // Use vue-resource or any other http library to send your request
}

function getUrlApi() {
  return Constantes.URL;
}

function callApi(url) {
  return new Promise((resolve, reject) => {
    axios
      .get(url)
      .then((response) => {
        resolve(response.data);
      })
      .catch(() => reject);
  });
}

export { getApiKey, getUrlApi, callApi };
