import firebase from "firebase/compat/app";
import "firebase/compat/storage";
import "firebase/compat/auth";
import "firebase/compat/firestore";

function signUp(email, password, name, img) {
  return new Promise((resolve, reject) => {
    firebase
      .auth()
      .createUserWithEmailAndPassword(email, password)
      .then((data) => {
        if (img != null) {
          data.user.updateProfile({
            displayName: name,
            photoURL: img,
          });
        } else {
          data.user.updateProfile({
            displayName: name,
          });
        }

        resolve(true);
      })
      .catch((err) => {
        reject(err);
        //this.error = err.message;
      });
  });
}

function signIn(email, password) {
  return new Promise((resolve, reject) => {
    firebase
      .auth()
      .signInWithEmailAndPassword(email, password)
      .then(() => {
        resolve(true);
      })
      .catch((err) => {
        reject(err);
      });
  });
}

function getCurrentUser() {
  return new Promise((resolve) => {
    firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        resolve(user);
        // ...
      } else {
        resolve();
      }
    });
  });
}

function getUidCurrentUser() {
  return new Promise((resolve) => {
    firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        resolve(user.uid);
        // ...
      } else {
        resolve(null);
      }
    });
  });
}

function signOut() {
  firebase.auth().signOut();
  return false;
}

function isConnected() {
  return new Promise((resolve) => {
    firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        resolve(true);
      } else {
        resolve(false);
      }
    });
  });
}

function storeImage(file) {
  return new Promise((resolve) => {
    var storageRef = firebase.storage().ref();
    var uploadTask = storageRef.child("Images/" + file.name).put(file);

    uploadTask.on("state_changed", () => {
      // Handle successful uploads on complete
      // For instance, get the download URL: https://firebasestorage.googleapis.com/...
      uploadTask.snapshot.ref.getDownloadURL().then((downloadURL) => {
        resolve(downloadURL);
      });
    });
  });
}

export {
  signUp,
  getCurrentUser,
  signOut,
  signIn,
  isConnected,
  getUidCurrentUser,
  storeImage,
};
