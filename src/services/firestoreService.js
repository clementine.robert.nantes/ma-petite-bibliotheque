import firebase from "firebase/compat/app";

const db = firebase.firestore();

async function getCompleteCollection(collectionName) {
  db.collection(collectionName).onSnapshot((snap) => {
    snap.forEach((doc) => {
      console.log(doc.data());
    });
  });
}

async function getUserBookByIdUserAndIdBook(idUser, idBook, collectionName) {
  return new Promise((resolve, reject) => {
    db.collection(collectionName)
      .where("idUser", "==", idUser)
      .where("idBook", "==", idBook)
      .get()
      .then((snapshot) => {
        snapshot.forEach((doc) => {
          resolve(doc);
        });
      })
      .catch((err) => reject(err));
  });
}

async function addUserBook(idUser, idBook, fav, wish, read, avis) {
  return new Promise((resolve, reject) => {
    if (avis) {
      db.collection("userBook")
        .add({
          idBook: idBook,
          idUser: idUser,
          favoris: fav,
          read: read,
          wish: wish,
          note: avis.note,
          avis: avis.avis,
          startDate: avis.startDate,
          endDate: avis.endDate,
        })
        .then((res) => {
          console.log("Ajout effectué avec succès");
          resolve(res);
        })
        .catch((err) => reject(err));
    } else {
      db.collection("userBook")
        .add({
          idBook: idBook,
          idUser: idUser,
          favoris: fav,
          read: read,
          wish: wish,
        })
        .then((res) => {
          console.log("Ajout effectué avec succès");
          resolve(res);
        })
        .catch((err) => reject(err));
    }
  });
}

async function addUserNoteBook(idUser, idBook, note, dateRead, avis) {
  db.collection("userNoteBook").add({
    idBook: idBook,
    idUser: idUser,
    avis: avis,
    note: note,
    dateRead: dateRead,
  });
}

async function isKnowBook(idUser, idBook) {
  return new Promise((resolve, reject) => {
    db.collection("userBook")
      .where("idUser", "==", idUser)
      .where("idBook", "==", idBook)
      .get()
      .then((snapshot) => {
        if (snapshot.size === 0) {
          resolve(false);
        } else {
          resolve(true);
        }
      })
      .catch((err) => reject(err));
  });
}

async function bookIsFav(idUser, idBook) {
  return new Promise((resolve, reject) => {
    db.collection("userBook")
      .where("idUser", "==", idUser)
      .where("idBook", "==", idBook)
      .where("favoris", "==", true)
      .get()
      .then((snapshot) => {
        if (snapshot.size === 0) {
          resolve(false);
        } else {
          resolve(true);
        }
      })
      .catch((err) => reject(err));
  });
}

async function updataData(relation, collectionName, type, avis = null) {
  return new Promise((resolve) => {
    let typeToReturn;
    switch (type) {
      case "wish": {
        db.collection(collectionName).doc(relation.id).update({
          wish: !relation.data().wish,
        });
        typeToReturn = !relation.data().wish;
        break;
      }

      case "favoris": {
        db.collection(collectionName).doc(relation.id).update({
          favoris: !relation.data().favoris,
        });
        typeToReturn = !relation.data().favoris;
        break;
      }

      case "read": {
        db.collection(collectionName).doc(relation.id).update({
          read: !relation.data().read,
        });
        typeToReturn = !relation.data().read;
        break;
      }

      case "note": {
        db.collection(collectionName).doc(relation.id).update({
          wish: false,
          read: true,
          note: avis.note,
          avis: avis.avis,
          startDate: avis.startDate,
          endDate: avis.endDate,
        });
        typeToReturn = null;
        break;
      }
    }
    resolve(typeToReturn);
  });
}

function createUserStat(idUser) {
  db.collection("userStat").add({
    idUser: idUser,
    nbFav: 0,
    nbNotes: 0,
    nbRead: 0,
    nbWish: 0,
  });
}

async function isStatUser(idUser) {
  return new Promise((resolve, reject) => {
    db.collection("userStat")
      .where("idUser", "==", idUser)
      .get()
      .then((snapshot) => {
        if (snapshot.size === 0) {
          resolve(false);
        } else {
          resolve(true);
        }
      })
      .catch((err) => reject(err));
  });
}

function getStatUser(user) {
  return new Promise((resolve) => {
    db.collection("userStat")
      .where("idUser", "==", user.uid)
      .get()
      .then((snapshot) => {
        if (snapshot.size !== 0) {
          snapshot.forEach((doc) => {
            user.nbNotes = doc.data().nbNotes;
            user.nbRead = doc.data().nbRead;
            user.nbWish = doc.data().nbWish;
            user.nbFav = doc.data().nbFav;
          });
        } else {
          user.nbNotes = 0;
          user.nbRead = 0;
          user.nbWish = 0;
          user.nbFav = 0;
        }
        resolve(user);
      });
  });
}

function getStat(idUser) {
  return new Promise((resolve) => {
    db.collection("userStat")
      .where("idUser", "==", idUser)
      .get()
      .then((snapshot) => {
        if (snapshot.size !== 0) {
          snapshot.forEach((doc) => {
            resolve(doc);
          });
        }
      });
  });
}
async function updateStatUser(idUser, type, operation) {
  const currentStat = await getStat(idUser);
  const currentStatData = currentStat.data();
  const idRelation = currentStat.id;

  let add = 0;
  if (operation) {
    add = 1;
  } else {
    add = -1;
  }

  switch (type) {
    case "wish": {
      db.collection("userStat")
        .doc(idRelation)
        .update({
          nbWish: currentStatData.nbWish + add,
        });

      break;
    }

    case "favoris": {
      db.collection("userStat")
        .doc(idRelation)
        .update({
          nbFav: currentStatData.nbFav + add,
        });
      break;
    }

    case "read": {
      db.collection("userStat")
        .doc(idRelation)
        .update({
          nbRead: currentStatData.nbRead + add,
        });
      break;
    }

    case "note": {
      db.collection("userStat")
        .doc(idRelation)
        .update({
          nbNotes: currentStatData.nbNotes + 1,
          nbRead: currentStatData.nbRead + 1,
        });
      break;
    }
  }
}

export {
  getCompleteCollection,
  getUserBookByIdUserAndIdBook,
  addUserBook,
  addUserNoteBook,
  isKnowBook,
  bookIsFav,
  updataData,
  getStatUser,
  createUserStat,
  isStatUser,
  updateStatUser,
};
