import { createRouter, createWebHistory } from "vue-router";
import Home from "../views/Home.vue";

const routes = [
  {
    path: '/index.html',
    component: Home,
    alias: '/',
  },
  {
    path: "/",
    name: "Home",
    component: Home,
  },
  {
    path: "/my-books",
    name: "MyBooks",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ "../views/MyBooks.vue"),
  },
  {
    path: "/scan-a-book",
    name: "ScanABook",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ "../views/ScanABook.vue"),
  },

  {
    path: "/search",
    name: "Search",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ "../views/Search.vue"),
  },

  {
    path: "/book-details/:isbnBook",
    name: "BookDetails",
    props: true,
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ "../views/DetailsBook.vue"),
  },

  {
    path: "/my-profile",
    name: "MyProfile",
    props: true,
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ "../views/MyProfile.vue"),
  },

  {
    path: "/note-page/:id",
    name: "NotePage",
    props: true,
    component: () => import("../views/NotePage.vue"),
  },

  {
    path: "/my-note-page/:id",
    name: "MyNotePage",
    props: true,
    component: () => import("../views/MyNotePage.vue"),
  },

  {
    path: "/books-creator/:id",
    name: "BooksCreator",
    props: true,
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ "../views/BooksCreator.vue"),
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
