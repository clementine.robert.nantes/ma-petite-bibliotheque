import { createApp } from "vue";
import App from "./App.vue";
import "./index.css";
import firebase from "firebase/compat/app";
import router from "./router";
import "./registerServiceWorker";

const firebaseConfig = {
  apiKey: "AIzaSyBhOVrLJzW2RTVaRj1LM6zTV1mJ6b6Xxls",
  authDomain: "bookedex-1eab1.firebaseapp.com",
  projectId: "bookedex-1eab1",
  storageBucket: "bookedex-1eab1.appspot.com",
  messagingSenderId: "1009959575429",
  appId: "1:1009959575429:web:d09539e27aee46079a6eec",
};

firebase.initializeApp(firebaseConfig);

createApp(App).use(router).mount("#app");
