module.exports = {
  purge: ["./index.html", "./src/**/*.{vue,js,ts,jsx,tsx}"],
  content: [],
  theme: {
    extend: {
      colors: {
        "black-gray": "#383838",
        "sky-blue": "#69E0F0",
        "subtil-gray": "#A1A1A1",
        "subtil-gray-hover": "#AAAAAA",
        "orange-book": "#FC5E03",
        "orange-book-hover": "#FF792C",
        "orange-from": "#FCE8DB",
        "orange-to": "#FCF7F4",
      },
    },
  },
  plugins: [],
};
